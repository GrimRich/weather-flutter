import 'package:flutter/material.dart';
import 'package:flutter_application_1/models/location.dart';

String _lat = "-6.919804076127623";
String _lon = "107.6184353749849";

void setCoor(lat, lon) {
  _lat = lat;
  _lon = lon;
}

List getCurrentCoor() {
  return [_lat, _lon];
}

List getLocation() {
  return _locations;
}

List getCoor(index) {
  return [
    _locations[index].lat,
    _locations[index].lon,
    _locations[index].title
  ];
}

Color latLonSame(int index) {
  if (_lat == _locations[index].lat && _lon == _locations[index].lon) {
    return Colors.amber;
  } else {
    return Colors.transparent;
  }
}

List<Location> _locations = [
  Location(
      title: "Bandung", lat: "-6.919804076127623", lon: "107.6184353749849"),
  Location(
      title: "Cimahi", lat: "-6.883445193189526", lon: "107.53837483501833"),
  Location(
      title: "Jakarta", lat: "-6.208567287684381", lon: "106.8404924969787"),
  Location(
      title: "Yogyakarta",
      lat: "-7.797126380288474",
      lon: "110.36797776408874"),
  Location(
      title: "Kediri", lat: "-7.8471778495045434", lon: "112.01481132827863"),
  Location(
    title: "Bantul",
    lat: "-7.875001465226321",
    lon: "110.32517733254262",
  ),
  Location(
    title: "New York",
    lat: "40.71178498005471",
    lon: "-74.00875501628586",
  ),
  Location(
    title: "Paris",
    lat: "48.85906361647457",
    lon: "2.2955595642141438",
  ),
  Location(
    title: "Manchester",
    lat: "53.47992814409042",
    lon: "-2.2440690876920213",
  ),
  Location(
    title: "Amsterdam",
    lat: "52.37095549734591",
    lon: "4.9059394353519234",
  )
];
