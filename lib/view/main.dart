import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/func/location.dart';
import 'package:flutter_application_1/func/time.dart';
import 'package:flutter_application_1/models/forecast_model.dart';
import 'package:flutter_application_1/models/location.dart';
import 'package:flutter_application_1/models/weather_model.dart';
import 'package:flutter_application_1/services/current_weather.dart';
import 'package:flutter_application_1/view/forecast/daily.dart';
import 'package:flutter_application_1/view/forecast/hourly.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  Future<WeatherModel> futureWeather;
  Future<ForecastModel> futureForecast;
  Location location;

  @override
  void initState() {
    fetch();
    super.initState();
  }

  void fetch() {
    futureWeather = fetchWeather(getCurrentCoor()[0], getCurrentCoor()[1]);
    futureForecast = fetchForecast(getCurrentCoor()[0], getCurrentCoor()[1]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        title: Text('Weather App'),
        actions: [
          TextButton.icon(
            onPressed: () {
              AdaptiveTheme.of(context).mode.name.toUpperCase() == 'DARK'
                  ? AdaptiveTheme.of(context).setLight()
                  : AdaptiveTheme.of(context).setDark();
            },
            icon: Icon(
              AdaptiveTheme.of(context).mode.name.toUpperCase() == 'DARK'
                  ? Icons.nightlight_round
                  : Icons.wb_sunny_rounded,
              color: Colors.white,
            ),
            label: Container(),
          )
        ],
      ),
      body: Container(
          margin: EdgeInsets.all(8.0),
          child: Column(
            children: [
              SizedBox(
                height: 8.0,
              ),
              FutureBuilder<WeatherModel>(
                  future: futureWeather,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Container(
                        child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    ListTile(
                                      leading: IconButton(
                                          icon: Icon(Icons.location_on),
                                          onPressed: () {
                                            alertLocation(context).show();
                                          }),
                                      title: Text(snapshot.data.name),
                                      subtitle: Text(getTime(snapshot.data)),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          children: [
                                            Container(
                                              margin:
                                                  EdgeInsets.only(left: 12.0),
                                              width: 50,
                                              height: 50,
                                              child: Image.network(
                                                  'https://openweathermap.org/img/w/${snapshot.data.weather[0].icon}.png'),
                                            ),
                                            SizedBox(
                                              width: 20.0,
                                            ),
                                            Text(
                                              "${snapshot.data.main.temp.toStringAsFixed(0)}°",
                                              style: TextStyle(
                                                  fontSize: 60,
                                                  fontWeight: FontWeight.w300),
                                            )
                                          ],
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: [
                                            Text(
                                                '${snapshot.data.weather[0].main}'),
                                            Text(
                                                '${snapshot.data.main.tempMax.toStringAsFixed(0)}° / ${snapshot.data.main.tempMin.toStringAsFixed(0)}°'),
                                            Text(
                                                'Feels like ${snapshot.data.main.feelsLike.toStringAsFixed(0)}°')
                                          ],
                                        )
                                      ],
                                    ),
                                    HourlyView(futureForecast: futureForecast)
                                  ],
                                ))),
                      );
                    } else {
                      return SizedBox(
                        child: CircularProgressIndicator(),
                        height: 60.0,
                        width: 60.0,
                      );
                    }
                  }),
              DailyView(futureForecast: futureForecast),
            ],
          )),
    );
  }

  Alert alertLocation(BuildContext context) {
    return Alert(
      context: context,
      title: "Pilih Lokasi",
      content: Container(
        height: 400,
        width: 400,
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: getLocation().length,
          itemBuilder: (BuildContext context, int index) => Container(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                  setState(() {
                    setCoor(getCoor(index)[0], getCoor(index)[1]);
                    fetch();
                    Navigator.pop(context);
                  });
                },
                child: ListTile(
                  tileColor: latLonSame(index),
                  title: Text(
                    getCoor(index)[2],
                  ),
                  subtitle: Text(getCoor(index)[0] + ' ,' + getCoor(index)[1]),
                ),
              ),
            ),
          ),
        ),
      ),
      buttons: [
        DialogButton(
          child: Text(
            "Close",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        )
      ],
    );
  }
}
