import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'weather_event.dart';
part 'weather_state.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  WeatherBloc() : super(WeatherInitial());

  @override
  Stream<WeatherState> mapEventToState(
    WeatherEvent event,
  ) async* {
    if (event is OpenDialog) {
      yield* _openDialog();
    } else if (event is CloseDialog) {
      yield* _closeDialog();
    }
  }

  Stream<WeatherState> _openDialog() async* {
    print('Open');
    yield Open();
  }

  Stream<WeatherState> _closeDialog() async* {
    print('Close');
    yield Close();
  }

  // @override
  // WeatherState get initialState => throw WeatherInitial();
}
