import 'dart:convert';

import 'package:flutter_application_1/models/weather_model.dart';
import 'package:flutter_application_1/models/forecast_model.dart';
import 'package:http/http.dart' as http;

Future<WeatherModel> fetchWeather(String lat, String lon) async {
  final String apiUrl =
      "https://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$lon&units=metric&appid=dced74bf1b19257e9ac7f373f457c25f";
  var response = await http.get(apiUrl);
  if (response.statusCode == 200) {
    return WeatherModel.fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to load weather');
  }
}

Future<ForecastModel> fetchForecast(String lat, String lon) async {
  final String apiUrl =
      "https://api.openweathermap.org/data/2.5/onecall?lat=$lat&lon=$lon&exclude=minutely,current&units=metric&appid=dced74bf1b19257e9ac7f373f457c25f";
  var response = await http.get(apiUrl);

  if (response.statusCode == 200) {
    return ForecastModel.fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to load weather');
  }
}
