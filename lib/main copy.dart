// import 'dart:async';
// import 'package:flutter_application_1/models/forecast_model.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_application_1/models/location.dart';

// import 'package:flutter_application_1/models/weather_model.dart';
// import 'package:intl/intl.dart';
// import 'package:flutter_application_1/services/current_weather.dart';

// void main() {
//   runApp(MyApp());
// }

// class MyApp extends StatefulWidget {
//   MyApp({Key key}) : super(key: key);

//   @override
//   _MyAppState createState() => _MyAppState();
// }

// class _MyAppState extends State<MyApp> {
//   Future<WeatherModel> futureWeather;
//   Future<ForecastModel> futureForecast;
//   Location location;

//   bool dialogLocation = false;

//   String lat = "-6.919804076127623";
//   String lon = "107.6184353749849";

//   @override
//   void initState() {
//     super.initState();

//     fetch();
//   }

//   void fetch() {
//     futureWeather = fetchWeather(lat, lon);
//     futureForecast = fetchForecast(lat, lon);
//   }

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//         debugShowCheckedModeBanner: false,
//         home: Scaffold(
//           appBar: AppBar(
//             backgroundColor: Colors.black87,
//             brightness: Brightness.dark,
//             title: Text('Weather App'),
//           ),
//           body: Stack(
//             children: [
//               Container(
//                   margin: EdgeInsets.all(8.0),
//                   child: Column(
//                     children: [
//                       SizedBox(
//                         height: 8.0,
//                       ),
//                       currentWeather(),
//                       forecastDay(),
//                     ],
//                   )),
//               dialogLocation
//                   ? Container(
//                       color: Colors.white,
//                       child: Column(
//                         children: [
//                           Flexible(
//                             flex: 11,
//                             child: ListView.builder(
//                                 shrinkWrap: true,
//                                 itemCount: locations.length,
//                                 itemBuilder: (BuildContext context,
//                                         int index) =>
//                                     Container(
//                                       child: Material(
//                                         color: Colors.transparent,
//                                         child: InkWell(
//                                           onTap: () {
//                                             setState(() {
//                                               lat = locations[index].lat;
//                                               lon = locations[index].lon;
//                                               dialogLocation = false;
//                                               fetch();
//                                             });
//                                           },
//                                           child: ListTile(
//                                               tileColor:
//                                                   lat == locations[index].lat &&
//                                                           lon ==
//                                                               locations[index]
//                                                                   .lon
//                                                       ? Colors.amber
//                                                       : Colors.transparent,
//                                               title:
//                                                   Text(locations[index].title),
//                                               subtitle: Text(
//                                                   locations[index].lat +
//                                                       " ," +
//                                                       locations[index].lon)),
//                                         ),
//                                       ),
//                                     )),
//                           ),
//                           Flexible(
//                             flex: 1,
//                             child: Row(
//                               mainAxisAlignment: MainAxisAlignment.end,
//                               children: [
//                                 Container(
//                                   margin: EdgeInsets.only(right: 12.0),
//                                   child: OutlinedButton(
//                                     onPressed: () {
//                                       setState(() {
//                                         dialogLocation = false;
//                                       });
//                                     },
//                                     child: Text("Close"),
//                                   ),
//                                 )
//                               ],
//                             ),
//                           )
//                         ],
//                       ),
//                     )
//                   : Container()
//             ],
//           ),
//         ));
//   }

//   FutureBuilder<WeatherModel> currentWeather() {
//     return FutureBuilder<WeatherModel>(
//         future: futureWeather,
//         builder: (context, snapshot) {
//           if (!snapshot.hasData) {
//             return CircularProgressIndicator();
//           }
//           return Container(
//             child: Card(
//                 shape: RoundedRectangleBorder(
//                   borderRadius: BorderRadius.circular(15.0),
//                 ),
//                 child: Padding(
//                     padding: const EdgeInsets.all(8.0),
//                     child: Column(
//                       mainAxisSize: MainAxisSize.min,
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: <Widget>[
//                         ListTile(
//                           leading: IconButton(
//                             icon: Icon(Icons.location_on),
//                             onPressed: () {
//                               setState(() {
//                                 dialogLocation = true;
//                               });
//                             },
//                           ),
//                           title: Text(snapshot.data.name),
//                           subtitle: Text(getTime(snapshot.data)),
//                         ),
//                         Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: [
//                             Row(
//                               children: [
//                                 Container(
//                                   margin: EdgeInsets.only(left: 12.0),
//                                   width: 50,
//                                   height: 50,
//                                   child: Image.network(
//                                       'https://openweathermap.org/img/w/${snapshot.data.weather[0].icon}.png'),
//                                 ),
//                                 SizedBox(
//                                   width: 20.0,
//                                 ),
//                                 Text(
//                                   "${snapshot.data.main.temp.toStringAsFixed(0)}°",
//                                   style: TextStyle(
//                                       fontSize: 60,
//                                       fontWeight: FontWeight.w300),
//                                 )
//                               ],
//                             ),
//                             Column(
//                               crossAxisAlignment: CrossAxisAlignment.end,
//                               children: [
//                                 Text('${snapshot.data.weather[0].main}'),
//                                 Text(
//                                     '${snapshot.data.main.tempMax.toStringAsFixed(0)}° / ${snapshot.data.main.tempMin.toStringAsFixed(0)}°'),
//                                 Text(
//                                     'Feels like ${snapshot.data.main.feelsLike.toStringAsFixed(0)}°')
//                               ],
//                             )
//                           ],
//                         ),
//                         forecastHour()
//                       ],
//                     ))),
//           );
//         });
//   }

//   forecastHour() {
//     return FutureBuilder<ForecastModel>(
//         future: futureForecast,
//         builder: (context, snapshot) {
//           if (!snapshot.hasData) {
//             return CircularProgressIndicator();
//           }
//           return Container(
//               height: 155,
//               padding: EdgeInsets.all(18),
//               margin: EdgeInsets.only(top: 18),
//               child: ListView.builder(
//                 shrinkWrap: true,
//                 scrollDirection: Axis.horizontal,
//                 itemCount: snapshot.data.hourly.length,
//                 itemBuilder: (BuildContext context, int index) => Container(
//                   height: 80.0,
//                   child: Container(
//                     margin: EdgeInsets.symmetric(horizontal: 18.0),
//                     child: Column(
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Text(getHour(snapshot.data.hourly[index])),
//                         Container(
//                             margin: EdgeInsets.only(top: 8.0),
//                             child: Image.network(
//                               'https://openweathermap.org/img/w/${snapshot.data.hourly[index].weather[0].icon}.png',
//                               height: 40.0,
//                             )),
//                         SizedBox(
//                           height: 5.0,
//                         ),
//                         Text(
//                           "${snapshot.data.hourly[index].weather[0].main}",
//                           style: TextStyle(fontSize: 14),
//                         ),
//                         SizedBox(
//                           height: 5.0,
//                         ),
//                         Text(
//                           "${snapshot.data.hourly[index].temp.toStringAsFixed(0)}°",
//                           style: TextStyle(fontSize: 14),
//                         ),
//                         SizedBox(
//                           height: 5.0,
//                         ),
//                         Row(
//                           mainAxisAlignment: MainAxisAlignment.center,
//                           children: [
//                             Icon(
//                               Icons.cloud_sharp,
//                               size: 15.0,
//                               color: Colors.blue[500],
//                             ),
//                             Text(
//                               ' ${snapshot.data.hourly[index].humidity}%',
//                               style: TextStyle(fontSize: 12),
//                             ),
//                           ],
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ));
//         });
//   }

//   forecastDay() {
//     return FutureBuilder<ForecastModel>(
//         future: futureForecast,
//         builder: (context, snapshot) {
//           if (!snapshot.hasData) {
//             return CircularProgressIndicator();
//           }
//           return Card(
//               shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.circular(15.0),
//               ),
//               child: Container(
//                   padding: EdgeInsets.symmetric(vertical: 18.0),
//                   child: ListView.builder(
//                     shrinkWrap: true,
//                     itemCount: snapshot.data.daily.length,
//                     itemBuilder: (BuildContext context, int index) => Column(
//                       children: [
//                         Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceAround,
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: [
//                             Container(
//                                 width: 60.0,
//                                 child: Text(
//                                     getOnlyDay(snapshot.data.daily[index]))),
//                             Flexible(
//                               flex: 1,
//                               child: Row(
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 children: [
//                                   Icon(
//                                     Icons.cloud_sharp,
//                                     size: 15.0,
//                                     color: Colors.blue[500],
//                                   ),
//                                   Text(
//                                     ' ${snapshot.data.daily[index].humidity.toStringAsFixed(0)}%',
//                                     style: TextStyle(fontSize: 12),
//                                   ),
//                                 ],
//                               ),
//                             ),
//                             Flexible(
//                               flex: 1,
//                               child: Container(
//                                 child: Image.network(
//                                   'https://openweathermap.org/img/w/${snapshot.data.daily[index].weather[0].icon}.png',
//                                   height: 30.0,
//                                 ),
//                               ),
//                             ),
//                             Flexible(
//                                 flex: 1,
//                                 child: Container(
//                                   margin: EdgeInsets.only(top: 8.0),
//                                   child: Text(
//                                       '${snapshot.data.daily[index].temp.max.toStringAsFixed(0)}°/${snapshot.data.daily[index].temp.min.toStringAsFixed(0)}°'),
//                                 )),
//                           ],
//                         )
//                       ],
//                     ),
//                   )));
//         });
//   }

//   String getTime(weather) {
//     var date =
//         DateTime.fromMillisecondsSinceEpoch(weather.dt * 1000, isUtc: true);

//     // DateTime zoneTime = date.add(Duration(seconds: weather.timezone ?? 0));

//     var formattedDate = DateFormat.yMMMMEEEEd().format(date);

//     return formattedDate;
//   }

//   String getHour(weather) {
//     var date =
//         DateTime.fromMillisecondsSinceEpoch(weather.dt * 1000, isUtc: true);

//     // DateTime zoneTime = date.add(Duration(seconds: weather.timezone ?? 0));

//     var formattedDate = DateFormat.j().format(date);

//     return formattedDate;
//   }

//   String getOnlyDay(weather) {
//     var date =
//         DateTime.fromMillisecondsSinceEpoch(weather.dt * 1000, isUtc: true);

//     // DateTime zoneTime = date.add(Duration(seconds: weather.timezone ?? 0));

//     var formattedDate = DateFormat.EEEE().format(date);

//     return formattedDate;
//   }
// }
